/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud2.classwork;

/**
 *
 * @author batoi
 */
public class Activitat9 {
    
    final static int RATIO_CONVERSION = 1024; 
            
    public static void main(String[] args) {
        int capacidad1 = 40000;
        int capacidad2 = 36000;
        
        System.out.println(capacidad1 + " Mb equivalen a " 
                + capacidad1 * RATIO_CONVERSION + " Kb");
        
        System.out.println(capacidad2 + " Mb equivalen a " 
                + capacidad2 * RATIO_CONVERSION + " Kb");
    }
    
}
