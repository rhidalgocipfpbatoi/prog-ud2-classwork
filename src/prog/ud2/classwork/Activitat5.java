/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud2.classwork;

/**
 *
 * @author batoi
 */
public class Activitat5 {
    public static void main(String[] args) {
        System.out.println("A   B   A&&B");
        System.out.println("------------");
        boolean A = false;
        boolean B = false;
        System.out.println(A + "   " + B + "   " + (A&&B));
        
        A = true;
        System.out.println(A + "   " + B + "   " + (A&&B));
        
        A = false;
        B = true;
        System.out.println(A + "   " + B + "   " + (A&&B));
        
        A = true;
        System.out.println(A + "   " + B + "   " + (A&&B));
    }
            
    
}
