/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud2.classwork;

/**
 *
 * @author batoi
 */
public class Activitat3 {
    
    public static void main(String[] args) {
        String nombre = "Roberto";
        String apellidos = "Hidalgo Martínez";
        byte edad = 100;
        boolean estaCasado = false;
        
        System.out.println(nombre + " " + apellidos);
        System.out.println("Tengo " + edad + " años");
        System.out.println(estaCasado);
    }
    
}
